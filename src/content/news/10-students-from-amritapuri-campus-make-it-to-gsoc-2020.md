---
title: "10 Students from Amritapuri Campus make it to GSoC 2020"
slug: "10-students-from-amritapuri-campus-make-it-to-gsoc-2020"
author: amfoss
cover: ./covers/gsoc-2020.png
date: "2020-05-04"
categories: ["Club Achievement"]
tags: ["gsoc", "google", "summer-of-code"]
pinned: true
description: "10 Students from Amrita Vishwa Vidyapeetham, Amritapuri Campus selected for Google Summer of Code 2020 Program."
---

Google Summer of Code is a global program focused on introducing students to open source software development. For the last 10 years, students from Amrita have been getting selected each year and so far, more than 60 students have made it as a GSoC Student Developer. This year 10 students have been accepted as Student Developers for various organisations.

Dubbed as the largest open-source event, GSoC requires interested students to submit proposals to one or two participating open source organisations, describing the details of the project that they wish to do for the organisation along with their past experiences. On the basis of this proposal, the organisation mentors select the most promising proposals and mentor the student for completing the project.

This year a total of 1,199 students were selected for the program, who will now be working on their projects with their respective organisations starting from June 1, throughout the summer till August 31.

The 10 students who got selected for the program and their respective organisations are -

1. Mayukh Deb - INCF

2. Shashank Priyadarshi - Mifos

3. Sashmita Raghav - KDE

4. Ashwin Ramakrishnan - Mifos

5. Yash Khare - Mifos

6. Shivangi Singh- Mifos

7. Suraj K Suresh - ReactOS

8. Aditya Vardhan - NetBSD

9. Ayushi Sharma - NetBSD

10. Venu Vardhan Reddy Tekula - CHAOSS
